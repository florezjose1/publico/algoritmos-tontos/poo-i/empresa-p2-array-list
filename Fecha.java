/**
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)
 * @author Colaborador: (Anthony Ardila 11519)
 */

class Fecha {

    private int dia;
    private int mes;
    private int anio;

    public static final int[] diasDelMes = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public Fecha(int dia, int mes, int anio) {
        //COMPLETE
        this.dia = dia;
        this.mes = mes;
        this.anio = anio;
    }

    public Fecha(int numero){
        //COMPLETE
        if(numero>0){
            this.dia=numero%100;
            this.mes=(numero/100)%100;
            this.anio=numero/10000;
        }
    }

    /**
     * @return the dia
     */
    public int getDia() {
        return dia;
    }

    /**
     * @param dia the dia to set
     */
    public void setDia(int dia) {
        this.dia = dia;
    }

    /**
     * @return the mes
     */
    public int getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(int mes) {
        this.mes = mes;
    }

    /**
     * @return the anio
     */
    public int getAnio() {
        return anio;
    }

    /**
     * @param anio the anio to set
     */
    public void setAnio(int anio) {
        this.anio = anio;
    }

    /**
     * Regresa true si ésta fecha es mayor que otra
     */
    public boolean esMayor(Fecha otra) {
        if(this.anio >= otra.anio){
            if(this.mes >= otra.mes) {
                return this.dia > otra.dia;
            }
        }
        return false;//COMPLETE
    }//fin esMayor

    /**
     * Regresa true si ésta fecha es igual que otra
     */
    public boolean esIgual(Fecha otra) {
        return this.anio == otra.anio && this.mes == otra.mes && this.dia == otra.dia;//COMPLETE
    }

    /**
     * Regresa true si ésta fecha es menor que otra
     */
    public boolean esMenor(Fecha otra) {
        return !this.esIgual(otra) && !this.esMayor(otra);//COMPLETE
    }

    public boolean esFechaValida() {
        boolean valida = false;
        if(this.dia <= this.diasDelMes[this.mes-1]){
            valida = true;
        }
        return valida;
    }

}
